const express = require('express');
const bodyParser = require('body-parser');
const botRoutes = require('./routes/callMessage');

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Setup template engine
app.set('view engine', 'ejs');
app.set('views', 'views');

app.use('/', botRoutes);
app.listen(3000);

module.exports = app;