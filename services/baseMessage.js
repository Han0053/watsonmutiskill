const config = require('../config/ibmConfig')[process.env.NODE_ENV || 'va1999'];
const AssistantV2 = require('ibm-watson/assistant/v2');
const { IamAuthenticator } = require('ibm-watson/auth');

class baseMessage {
    constructor(config) {
        this.service = new AssistantV2({
            version: config.version,
            authenticator: new IamAuthenticator({
                apikey: config.apikey,
            }),
            url: config.url,
        });
    }

    async
}

module.exports = baseMessage();