const config = require('../config/ibmConfig')[process.env.NODE_ENV || 'va1999'];
const AssistantV2 = require('ibm-watson/assistant/v2');
const { IamAuthenticator } = require('ibm-watson/auth');

class domainController {
    constructor() {
        this.createNewDomain = this.createNewDomain.bind(this);
        this.getDomainSession = this.getDomainSession.bind(this);
        this.postMessage = this.postMessage.bind(this);
        this.deleteDomainSession = this.deleteDomainSession.bind(this);
        this.domainInit = this.domainInit.bind(this);
    }

    async inDomain(currObj, message) {

        // If session delete yet
        if (currObj.Current_Session_ID) {
            console.log('使用原session: ', currObj.Current_Session_ID);

            //let domainAns = await domainMessage.postMessage(currObj, message);
            let domainAns = await this.postMessage(currObj, message);

            if (!domainAns.hasOwnProperty('result')) {
                console.log('The watson session is timeout....');
                currObj.Current_Session_ID = null;
                console.log('確認刪除session: ', currObj.Current_Session_ID);
                return await this.createNewDomain(currObj, message);
            } else {
                let domainContext = domainAns.result.output;
                if (domainContext.intents.length == 0) {
                    await this.deleteDomainSession(currObj);
                    currObj.Current_Session_ID = null;
                    console.log('確認session被刪除: ', currObj.Current_Session_ID)
                    return domainContext;
                }
                return domainContext;
            }
        }
        return await this.createNewDomain(currObj, message);
    }

    async createNewDomain(currObj, message) {
        let domainSession;

        // Get Domain Session & keep it
        let getDomainSession = await this.getDomainSession(currObj.Current_Skill_ID);
        domainSession = getDomainSession.result.session_id;
        console.log('This is new domain session: ', domainSession);
        currObj.Current_Session_ID = domainSession;
        console.log('確認是否keep住session: ', currObj.Current_Session_ID);

        // Get Answer
        //let domainAns = await domainMessage.postMessage(currObj, message);
        let domainAns = await this.postMessage(currObj, message);
        let answerText = JSON.parse(domainAns.result.output.generic[0].text);
        return answerText.ans_id;
    }

    // Authenticate
    domainInit(skill) {
        const service = new AssistantV2({
            version: config.skills[skill].version,
            authenticator: new IamAuthenticator({
                apikey: config.skills[skill].apikey,
            }),
            url: config.skills[skill].url,
        });
        return service;
    }

    // Get domain session data
    async getDomainSession(skill) {
        return await this.domainInit(skill).createSession({
            assistantId: config.skills[skill].assistantId
        });
    };

    // Post message to Domain Skill and get response
    async postMessage(currObj, msg) {
        try {
            return await this.domainInit(currObj.Current_Skill_ID).message({
                assistantId: config.skills[currObj.Current_Skill_ID].assistantId,
                sessionId: currObj.Current_Session_ID,
                input: {
                    "message_type": "text",
                    "text": msg
                }
            });
        } catch (error) {
            if (error.message == 'Invalid Session') {
                return error;
            }
        }
    };

    // Delete domain session
    async deleteDomainSession(currObj) {
        return await this.domainInit(currObj.Current_Skill_ID).deleteSession({
            assistantId: config.skills[currObj.Current_Skill_ID].assistantId,
            sessionId: currObj.Current_Session_ID
        });
    }
}

module.exports = new domainController();