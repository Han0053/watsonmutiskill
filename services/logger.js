const SimpleNodeLogger = require('simple-node-logger');

class logger {
    constructor() {
        this.init = this.init.bind(this);
        this.log = this.log.bind(this);
        this.error = this.error.bind(this);
    }

    init(fileName) {
        this.fileName = fileName;

        this.opts1 = {
            logDirectory: './logs',
            fileNamePattern: `${this.fileName}-<DATE>.log`,
            dateFormat: 'YYYY-MM-DD',
            timestampFormat: 'YYYY-MM-DD HH:mm:ss.SSS'
        };
        this.normalLogger = SimpleNodeLogger.createRollingFileLogger(this.opts1);

        this.opts2 = {
            logDirectory: './logs',
            fileNamePattern: `${this.fileName}-error-<DATE>.log`,
            dateFormat: 'YYYY-MM-DD',
            timestampFormat: 'YYYY-MM-DD HH:mm:ss.SSS'
        };
        this.errorLogger = SimpleNodeLogger.createRollingFileLogger(this.opts2);
    }
    log() {
        let mylog = `${Object.values(arguments).join(' ')}`;

        this.normalLogger.info(mylog);
    }
    error() {
        let mylog = `${Object.values(arguments).join(' ')}`;

        this.errorLogger.error(mylog);
    }
}
module.exports = new logger();