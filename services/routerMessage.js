const config = require('../config/ibmConfig')[process.env.NODE_ENV || 'va1999'];
const domainController = require('./domainMessage');
const AssistantV2 = require('ibm-watson/assistant/v2');
const { IamAuthenticator } = require('ibm-watson/auth');
const mysql = require('../dao/mysqlConn');
const vaInfo = require('../model/vaInfo');

const chnlMapRouterID = {};

class routerController {
    constructor() {
        this.createNewSkill = this.createNewSkill.bind(this);
        this.getRouterSession = this.getRouterSession.bind(this);
        this.getSkillObject = this.getSkillObject.bind(this);
        this.routerInit = this.routerInit.bind(this);
    }

    async inRouter(currObj, message, channel) {

        console.log('This is current object: ', currObj);

        let currSkill = currObj.Current_Skill_ID;

        if (currSkill) {
            console.log('進入原skill進行對話: ', currSkill);
            let response = await domainController.inDomain(currObj, message);

            if (response.intents.length == 0) {
                currObj.Current_Skill_ID = null;
                console.log('確認skill被刪除: ', currObj.Current_Skill_ID);
                return await this.createNewSkill(currObj, message, channel);
            }
            return JSON.parse(response.generic[0].text).ans_id;
        }
        // Create new skill
        return await this.createNewSkill(currObj, message, channel);
    }

    async createNewSkill(currObj, message, channel) {

        let routerSession;

        // Query Router_ID & Keep it
        if (!chnlMapRouterID[channel]) {
            await mysql.query(channel).then(result => {
                chnlMapRouterID[channel] = result;
            });
            console.log('確認是否keep住router id: ', chnlMapRouterID[channel]);
            console.log(chnlMapRouterID);
        };

        // Get Router Session
        let getRouterSession = await this.getRouterSession(channel);
        routerSession = getRouterSession.result.session_id;
        console.log('This is new router session: ', routerSession);

        // Get Skill ID
        let routerAns = await this.getSkillObject(message, routerSession, channel);
        if (routerAns.result.output.intents.length == 0) {
            return '對不起，我無法理解您得意思'
        }
        let routerContext = JSON.parse(routerAns.result.output.generic[0].text);

        // Keep skill
        let newSkill = routerContext.skill_id;
        console.log('This is new skill id: ', newSkill);
        currObj.Current_Skill_ID = newSkill;
        console.log('確認是否keep住skill id: ', currObj.Current_Skill_ID);

        // Keep chain
        //currObj.Chain = await this.queryChain(currObj.Current_Skill_ID);

        return await domainController.inDomain(currObj, message);
    }

    // Authenticate
    routerInit(channel) {
        const service = new AssistantV2({
            version: config[chnlMapRouterID[channel]].version,
            authenticator: new IamAuthenticator({
                apikey: config[chnlMapRouterID[channel]].apikey,
            }),
            url: config[chnlMapRouterID[channel]].url,
        });
        return service;
    }

    // Get router session data
    async getRouterSession(channel) {
        return await this.routerInit(channel).createSession({
            assistantId: config[chnlMapRouterID[channel]].assistantId
        });
    }

    async getSkillObject(msg, routerSession, channel) {
        return await this.routerInit(channel).message({
            assistantId: config[chnlMapRouterID[channel]].assistantId,
            sessionId: routerSession,
            input: {
                "message_type": "text",
                "text": msg
            }
        });
    }
}

module.exports = new routerController();