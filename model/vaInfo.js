class vaInfo {
    constructor() {

    }

    get skillConfig() {
        return this._skillConfig;
    }
    set skillConfig(config) {
        this._skillConfig = config;
    }

    get resMsg() {
        return this._resMsg;
    }
    set resMsg(msg) {
        this._resMsg = msg;
    }
}