const mysql = require('mysql');
const config = require('../config/sqlConfig');

const QUERY_ROUTER_ID = "SELECT ROUTER_SKILL_ID FROM CHANNEL WHERE CHANNEL_ID = ?";

class mysqlConnect {
    constructor() {
        this.con = mysql.createConnection({
            host: config.host,
            user: config.user,
            password: config.password,
            database: 'mysql'
        });

        this.con.connect(function (err) {
            if (err) {
                console.log('Connect error: ', err);
                return;
            }
            console.log('Connect success...');
            return this.con;
        });
    }

    query(channel) {
        return new Promise((resolve, reject) => {
            this.con.query(QUERY_ROUTER_ID, [channel], (err, rows) => {
                if (err) reject(err);
                else {
                    resolve(rows[0].ROUTER_SKILL_ID);
                }
            });
        });
    }
};

module.exports = new mysqlConnect();