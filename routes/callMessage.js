const express = require('express');
const session = require('express-session');
const routerController = require('../services/routerMessage');
const currentDomainSkill = require('../model/currentDomainSkill');

// express session
const router = express.Router();

router.use(session({
    secret: 'recommand 128 bytes random string',
    resave: false,
    saveUninitialized: true
}));

router.post('/myMessage', async function (req, res, next) {
    let message = req.body.input.text;
    let channel = req.body.input.channel;

    if (!req.session.currentDomainSkill) {
        req.session.currentDomainSkill = currentDomainSkill;
    }
    let currentObj = req.session.currentDomainSkill

    let response = await routerController.inRouter(currentObj, message, channel);
    console.log('The user say: ', message);
    console.log('This is final response: ', response);
    res.json(response);
});

module.exports = router;
