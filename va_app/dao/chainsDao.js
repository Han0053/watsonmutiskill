const sql = require('../config/sqlConfig');

class chainsDao {
    constructor() {
    }
    async getChains(skillId) {
        let chain = [];

        let sqlRes = await sql.SKILL_CHAIN.findAll({
            where: {
                "SKILL_ID": skillId
            },
            attributes: ["NEXT_SKILL_ID"],
            order: [
                ['PRIORITY', 'ASC']
            ],
        });

        for (let i in sqlRes) {
            chain.push(sqlRes[i].dataValues);
        }
        return chain;
    }
}
module.exports = new chainsDao();