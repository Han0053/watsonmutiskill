const sql = require('../config/sqlConfig');

class answerPackDao {
    constructor() {
    }
    async getAnswerPack(answerId) {
        let sqlRes = await sql.ANSWER_PACK.findAll({
            where: {
                "ANSWER_ID": answerId
            },
            attributes: ["CONTENT"]
        });
        return sqlRes[0].dataValues.CONTENT;
    }
}
module.exports = new answerPackDao();