const sql = require('../config/sqlConfig');

class routerIdDao {
    constructor() {
    }
    async getRouterId(channel) {
        let sqlRes = await sql.CHANNEL.findAll({
            where: {
                "CHANNEL_ID": channel
            },
            attributes: ["ROUTER_SKILL_ID"]
        });
        return sqlRes[0].dataValues.ROUTER_SKILL_ID;
    }
}
module.exports = new routerIdDao();