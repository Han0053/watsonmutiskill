const sql = require('../config/sqlConfig');

class skillPoolDao {
    constructor() {
    }

    async getskillPool(skillId) {
        let sqlRes = await sql.SKILL_POOL.findAll({
            where: {
                "SKILL_ID": skillId
            },
            attributes: ['SKILL_CONFIG']
        });
        return sqlRes[0].SKILL_CONFIG;
    }

    async hasChain(skillId) {
        let sqlRes = await sql.SKILL_POOL.findAll({
            where: {
                "SKILL_ID": skillId
            },
            attributes: ['HAS_CHAIN']
        });
        return sqlRes[0].HAS_CHAIN;
    }
}
module.exports = new skillPoolDao();