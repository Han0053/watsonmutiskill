const Sequelize = require('sequelize');
const sequelize = require('./database');

// Data structure define
const SKILL_CHAIN = sequelize.define('SKILL_CHAIN', {
    SKILL_ID: {
        type: Sequelize.STRING(50),
        allowNull: false,
    },
    NEXT_SKILL_ID: Sequelize.STRING(50),
    PRIORITY: Sequelize.INTEGER,
    ON_DUTY: Sequelize.STRING(1),
    MEMO: Sequelize.STRING(1500)
}, { freezeTableName: true });

const ANSWER_PACK = sequelize.define('ANSWER_PACK', {
    ANSWER_ID: {
        type: Sequelize.STRING(15),
        allowNull: false,
        primaryKey: true
    },
    CONTENT: {
        type: Sequelize.STRING(1500),
        allowNull: false
    },
    ON_DUTY: Sequelize.STRING(1),
    MEMO: Sequelize.STRING(1500)
}, { freezeTableName: true });

const SKILL_POOL = sequelize.define('SKILL_POOL', {
    SKILL_ID: {
        type: Sequelize.STRING(50),
        allowNull: false,
        primaryKey: true
    },
    SKILL_NAME: Sequelize.STRING(100),
    SKILL_CONFIG: Sequelize.STRING(1500),
    PROVIDER: Sequelize.STRING(20),
    HAS_CHAIN: Sequelize.STRING(1),
    ON_DUTY: Sequelize.STRING(1),
    MEMO: Sequelize.STRING(1500)
}, { freezeTableName: true });

const CHANNEL = sequelize.define('CHANNEL', {
    CHANNEL_ID: {
        type: Sequelize.STRING(20),
        allowNull: false,
        primaryKey: true
    },
    CHANNEL_NAME: Sequelize.STRING(100),
    ROUTER_SKILL_ID: Sequelize.STRING(50),
    ON_DUTY: Sequelize.STRING(1),
    MEMO: Sequelize.STRING(1500)
}, { freezeTableName: true });

module.exports = { SKILL_CHAIN, ANSWER_PACK, SKILL_POOL, CHANNEL };