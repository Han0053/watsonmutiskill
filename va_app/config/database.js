const Sequelize = require('sequelize');
const Config = require('./dbConfig');

const sequelize = new Sequelize(Config.db.database, Config.db.account, Config.db.pwd, {
    host: Config.db.host,
    port: Config.db.port,
    dialect: 'mssql',
    dialectOptions: {
        options: {
            encrypt: Config.db.encrypt
        }
    }
});

module.exports = sequelize;