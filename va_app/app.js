const express = require('express');
const session = require('express-session');
const bodyParser = require('body-parser');
const cors = require('cors');
const sequelize = require('./config/database');
const Config = require('./config/dbConfig');

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

var corsOptions = {
    "origin": "http://localhost:3001",
    "methods": "GET, HEAD, PUT, PATHCH, POST, DELETE",
    "preflightContinue": false,
    "optionsSuccessStatus": 204
}

app.use(cors(corsOptions));

//set session
app.use(session({
    secret: 'expressSession',
    resave: false,
    saveUninitialized: true,
}))

// Routes
const MessageRoutes = require('./routes/messageAPI');
app.use('/bot', MessageRoutes);

const AnsPackRoutes = require('./routes/answerpackAPI');
app.use(AnsPackRoutes);

// Connecting MSSQL.
sequelize
    .sync()
    .then(() => {
        console.log('Connect to SQL DB-------------------------------------------------------------------');
        app.listen(Config.webPort);
    })
    .catch(err => {
        console.log(err);
    });
