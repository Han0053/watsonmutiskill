const express = require('express');
const multipleSkillsService = require('../services/multipleSkillsService');
const currentDomainSkill = require('../models/multipleSkillSession');
const answerPackService = require('../services/answerpackService');
const router = express.Router();

router.post('/message', async (req, res, next) => {
    let message = req.body.input.text;
    let channel = req.body.input.channel;
    if (!req.session.currentDomainSkill) {
        req.session.currentDomainSkill = currentDomainSkill;
    }
    let currentObj = req.session.currentDomainSkill;
    let answer_id = await multipleSkillsService.dialog(currentObj, message, channel);
    let answerPack = await answerPackService.callAnsPack(answer_id);

    console.log("User say: ", message);
    console.log("Ans : ", answer_id);
    res.json(answerPack);

    console.log("The answerPack : ", answerPack);
});
module.exports = router;
