const express = require('express');

const answerPackDao = require('../dao/answerPackDao');

const router = express.Router();

router.get('/ansPackAPI/:id', async (req, res, next) => {
    const logId = req.params.id;
    let answerPack = await answerPackDao.getAnswerPack(logId);
});

module.exports = router;