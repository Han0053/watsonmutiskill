const NodeCache = require("node-cache");

const configCache = new NodeCache();
const ansPackCache = new NodeCache();
const routerCache = new NodeCache();

class vaCache {
    constructor() {
    }

    setRouterID(key, value) {
        routerCache.set(key, value, 1000);
    }

    getRouterID(key) {
        return routerCache.get(key);
    }

    setConfig(key, value) {
        configCache.set(key, value, 1000);
    }

    getConfig(key) {
        return configCache.get(key);
    }

    setAnsPack(key, value) {
        ansPackCache.set(key, value, 1000);
    }

    getAnsPack(key) {
        return ansPackCache.get(key);
    }
}

module.exports = new vaCache();