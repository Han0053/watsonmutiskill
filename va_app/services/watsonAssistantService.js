const AssistantV2 = require('ibm-watson/assistant/v2');
const { IamAuthenticator } = require('ibm-watson/auth');
const cache = require('../models/vaCache');

class commSkillMethod {
    constructor() {
        this.init = this.init.bind(this);
    }

    // Initial ibm-watson connection details
    init(id) {
        let config = cache.getConfig(id);
        return new AssistantV2({
            version: config.VERSION,
            authenticator: new IamAuthenticator({
                apikey: config.API_KEY,
            }),
            url: config.API_URL,
        });
    }

    // Create common skill session
    async createSession(id) {
        return await this.init(id).createSession({
            assistantId: cache.getConfig(id).ASSISTANT_ID
        });
    }

    // Post message to common skill 
    async postMessage(id, session, message) {
        try {
            return await this.init(id).message({
                assistantId: cache.getConfig(id).ASSISTANT_ID,
                sessionId: session,
                input: {
                    "message_type": "text",
                    "text": message
                }
            })
        } catch (err) {
            if (err.message == 'Invalid Session') {
                return err;
            }
        }
    }

    //Delete common skill session
    async deleteSession(id, session) {
        return await this.init(id).deleteSession({
            assistantId: cache.getConfig(id).ASSISTANT_ID,
            sessionId: session
        });
    }
}

module.exports = new commSkillMethod();