const answerPackDao = require('../dao/answerPackDao');
const cache = require('../models/vaCache');

class answerPackService {
    constructor() {
    }

    async callAnsPack(answerID) {
        let answerPack;

        if (!await cache.getAnsPack(answerID)) {
            cache.setAnsPack(answerID, JSON.parse(await answerPackDao.getAnswerPack(answerID)));
        }

        answerPack = await cache.getAnsPack(answerID);
        return answerPack;
    }
}

module.exports = new answerPackService;