const domain = require('../services/domainSkillService');

class skillChainService {
    constructor() {
    }

    async domainSkillChain(currentObj, message) {
        let answer_id;
        for (let j = 0; j < currentObj.Chain.length; j++) {
            if (currentObj.Domain_Skill_ID == currentObj.Chain[j]) {
                continue;
            }
            currentObj.Current_Skill_ID = currentObj.Chain[j];
            console.log("Skill : ", currentObj.Current_Skill_ID);
            answer_id = await domain.createNewDialog(currentObj, message);
            if (answer_id == 'Als_1') {
                continue;
            }
            break;
        }
        return answer_id;
    }
}

module.exports = new skillChainService();