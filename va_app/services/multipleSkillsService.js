const router = require('./routerSkillService');
const domain = require('./domainSkillService');

const ROUTER_NO_ANS = 'Sys_2';
const ANYTHING_ELSE = 'Als_1';

class multipleSkillsService {
    constructor() {
        this.dialog = this.dialog.bind(this);
    }

    async multipleSkill(currentObj, message, channel) {

        let answer_id;
        let skill_id = currentObj.Current_Skill_ID;

        // First dialog
        if (!skill_id) {
            return this.dialog(currentObj, message, channel);
        }

        // Run current skill
        answer_id = await domain.dialog(currentObj, message);
        if (answer_id != ANYTHING_ELSE) {
            return answer_id;
        }

        currentObj.Domain_Skill_ID = null;
        currentObj.Current_Skill_ID = null;
        currentObj.Current_Session_ID = null;
        currentObj.Chain.length = 0;
        console.log("Empty obj: ", currentObj);

        return this.dialog(currentObj, message, channel);
    }

    async dialog(currentObj, message, channel) {
        let routerRes = await router.dialog(channel, message);
        if (routerRes == ROUTER_NO_ANS) {
            return routerRes;
        }
        currentObj.Domain_Skill_ID = routerRes;
        currentObj.Current_Skill_ID = routerRes;
        let answer_id = await domain.dialog(currentObj, message);
        return answer_id;
    }
}
module.exports = new multipleSkillsService();