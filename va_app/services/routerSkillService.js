const cache = require('../models/vaCache');
const skillMethod = require('./watsonAssistantService');

const skillPoolDao = require('../dao/skillPoolDao');
const routerIdDao = require('../dao/routerIdDao');

class routerSkillService {
    constructor() {
        this.setRouterID = this.setRouterID.bind(this);
    }

    // Go to router skill
    async dialog(channel, message) {

        // First dialog with watson
        let router_id = await cache.getRouterID(channel);

        if (!router_id) {
            router_id = await this.setRouterID(channel);
        }

        if (!await cache.getConfig(router_id)) {
            cache.setConfig(router_id, JSON.parse(await skillPoolDao.getskillPool(router_id)));
        }

        let getRouterSession = await skillMethod.createSession(router_id);
        let session = getRouterSession.result.session_id;

        let routerRes = await skillMethod.postMessage(router_id, session, message);
        let resText = JSON.parse(routerRes.result.output.generic[0].text);

        if (!resText.hasOwnProperty('skill_id')) {
            let answer_id = resText.answer_id;
            return answer_id;
        }
        let newSkill = resText.skill_id;

        await skillMethod.deleteSession(router_id, session);

        return newSkill;
    }

    async setRouterID(channel) {
        let router_id = await routerIdDao.getRouterId(channel);
        cache.setRouterID(channel, router_id);
        return router_id;
    }
}

module.exports = new routerSkillService();