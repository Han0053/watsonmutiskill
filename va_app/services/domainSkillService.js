const skillMethod = require('./watsonAssistantService');
const cache = require('../models/vaCache');
const ANYTHING_ELSE = 'Als_1';

const skillPoolDao = require('../dao/skillPoolDao');

class domainSkillService {
    constructor() {
        this.dialog = this.dialog.bind(this)
    }

    // Ready to go common skill
    async domainSkill(currentObj, message) {

        let skill_id = currentObj.Current_Skill_ID;
        let session = currentObj.Current_Session_ID;
        let domainRes;
        let answer_id;

        if (!session) {
            return await this.dialog(currentObj, message);
        }

        if (!await cache.getConfig(skill_id)) {
            cache.setConfig(skill_id, JSON.parse(await skillPoolDao.getskillPool(skill_id)));
        }

        domainRes = await skillMethod.postMessage(skill_id, session, message);
        if (!domainRes.hasOwnProperty('result')) {
            console.log("The watson session is timeout, recreate new session...");
            currentObj.Current_Session_ID = null;
            return await this.dialog(currentObj, message);
        }

        answer_id = JSON.parse(domainRes.result.output.generic[0].text).ansId;

        if (answer_id != ANYTHING_ELSE) {
            return answer_id;
        }

        await skillMethod.deleteSession(skill_id, session);
        currentObj.Current_Session_ID = null;

        let hasChain = await skillPoolDao.hasChain(currentObj.Domain_Skill_ID);

        if (hasChain == 'N') {
            return answer_id;
        }
        let getChains = await chainsDao.getChains(newSkill);
        for (let i in getChains) {
            currentObj.Chain.push(getChains[i].NEXT_SKILL_ID);
        }
        answer_id = await doaminChain.domainSkillChain(currentObj, message);
        return answer_id;
    }

    // Go to common skill
    async dialog(currentObj, message) {
        let id = currentObj.Current_Skill_ID;
        let session;
        let domainRes;
        let answer_id;

        if (!await cache.getConfig(id)) {
            cache.setConfig(id, JSON.parse(await skillPoolDao.getskillPool(id)));
        }

        let createSessionObj = await skillMethod.createSession(id);
        session = createSessionObj.result.session_id;
        currentObj.Current_Session_ID = session;
        console.log('Keep session in obj: ', currentObj);

        domainRes = await skillMethod.postMessage(id, session, message);
        answer_id = JSON.parse(domainRes.result.output.generic[0].text).ansId;
        return answer_id;
    }
}
module.exports = new domainSkillService();